﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MJMaze.MazeRepresentation;

namespace MJMaze.IO
{
    public class FileReader
    {
        public FileReader()
        {
            
        }
        
        //it might be reasonable to put here AbstractClass with proccess function instead of Action
        public void OpenFileAndProccessWith(string path, Action proccess)
        {
            using (TextReader readFile = new StreamReader(path))
            {
                proccess.Invoke();
            }
        }

        public MazeSolver<int[][]> OpenFileAndProccessWith(string path, Func<TextReader,MazeSolver<int[][]>> proccessToMazeTask)
        {
            MazeSolver<int[][]> result;
            using (TextReader readFile = new StreamReader(path))
            {
                result = proccessToMazeTask.Invoke(readFile);
            }
            return result;
        }
    }
    public class InputReader<TIn,TOut> : InOutMapper<TIn,TOut>
    {
    }

    public class InOutMapper<TIn, TOut>
    {
        //TOut InputMapper(TIn input) { return new TOut();}
    }
}