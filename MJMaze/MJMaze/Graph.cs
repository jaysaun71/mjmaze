﻿using System;
using System.Collections.Generic;
using MJMaze.MazeRepresentation;

namespace MJMaze
{
    public class Graph
    {
        private List<int>[] nodes;

        public Graph(int size)
        {
            this.nodes = new List<int>[size];
            for (int i = 0; i < size; i++)
            {
                this.nodes[i] = new List<int>();
            }
        }
        public Graph(List<int>[] nodes)
        {
            this.nodes = nodes;
        }

        public int Size => this.nodes.Length;

        public void AddEdge(int u, int v)
        {
            nodes[u].Add(v);
        }

        public void RemoveEdge(int u, int v)
        {
            nodes[u].Remove(v);
        }

        public bool HasEdge(int u, int v)
        {
            bool hasEdge = nodes[u].Contains(v);
            return hasEdge;
        }

        public IList<int> GetNeighbours(int v)
        {
            return nodes[v];
        }
    }
}