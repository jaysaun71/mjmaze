﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MJMaze.IO;
using MJMaze.Mapper;
using MJMaze.MazeRepresentation;

namespace MJMaze
{
    class Program
    {
        static void Main()
        {
            string inputPath = ReadArguments();
            var fileReader = new FileReader();
            var mazeRepresentation =
                fileReader.OpenFileAndProccessWith(inputPath,
                    TextReaderToMazeRepresentationMapper.MazeReprezentationFrom);

            var graph = ArrayMazeToGraphMapper.ArrayMazeToGraph(mazeRepresentation.Maze, mazeRepresentation.Size.y, mazeRepresentation.Size.x);

            var shortestPath = mazeRepresentation.SearchPath(graph,
                mazeRepresentation.StartPoint.y * mazeRepresentation.Size.x + mazeRepresentation.StartPoint.x,
                mazeRepresentation.StopPoint.y * mazeRepresentation.Size.x + mazeRepresentation.StopPoint.x);

            WriteToOutput(mazeRepresentation, shortestPath);

        }

        private static void WriteToOutput(MazeSolver<int[][]> maze, List<int> shortestPath)
        {
            int width = maze.Size.x;
            int height = maze.Size.y;
            string[][] outputMaze = new string[height][];
            SetOutputMazeArray(width, height, outputMaze);


            // add '#'
            for (int i = 0; i < maze.Maze.Length; i++)
            {
                for (int j = 0; j < maze.Maze[i].Length; j++)
                {
                    outputMaze[i][j] = (maze.Maze[i][j] == 1) ? "#" : " ";
                }
            }

            foreach (var sp in shortestPath)
            {
                int y = (sp / width);
                int x = sp - (y*width);
                outputMaze[y][x] = "X";
            }

            //Start point
            outputMaze[maze.StartPoint.y][maze.StartPoint.x] = "S";
            //Stop point 
            outputMaze[maze.StopPoint.y][maze.StopPoint.x] = "E";

            WriteStringArrayToConsoleOutput(outputMaze);
        }

        public static void WriteStringArrayToConsoleOutput(string[][] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array[i].Length; j++)
                {
                    Console.Write(array[i][j]);
                }
                Console.WriteLine();
            }
        }

        private static void SetOutputMazeArray(int width, int height, string[][] outputMaze)
        {
            for (int i = 0; i < height; i++)
            {
                outputMaze[i] = new string[width];
            }
        }

        private static string ReadArguments()
        {
            String[] arguments = Environment.GetCommandLineArgs();
            Console.WriteLine("GetCommandLineArgs: {0}", String.Join(", ", arguments));

            return (arguments.Length > 1) ? arguments[1] : string.Empty;
        }
    }
}
