﻿using System;
using System.Collections.Generic;

namespace MJMaze.MazeRepresentation
{
    public interface IPathSearchable
    {
       List<int> SearchPath(Graph graph, int root, int goal);
    }
}