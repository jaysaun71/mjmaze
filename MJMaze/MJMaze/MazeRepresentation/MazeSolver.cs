﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace MJMaze.MazeRepresentation
{
    public class MazeSolver<TGraph> : IMazeSolver<TGraph>
    {
        private int[] nodeWeights;
        public SimplePoint Size { get; set; }
        public SimplePoint StartPoint { get; set; }
        public SimplePoint StopPoint { get; set; }
        public TGraph Maze { get; set; }

        /// <summary>
        /// Breadth-first search algorithm
        /// </summary>
        /// <param name="graph">graph representing our maze</param>
        /// <param name="root">the start point of maze</param>
        /// <param name="goal">the end point of maze</param>
        public List<int> SearchPath(Graph graph, int root, int goal)
        {
            List<int> visitedNodes = new List<int>();
            Queue<int> queue = new Queue<int>();
            nodeWeights = new int[graph.Size];
            int distanceFromStart = 1;

            visitedNodes.Add(root);
            queue.Enqueue(root);
            nodeWeights[root] = distanceFromStart;

            while (queue.Count != 0)
            {
                var currentNode = queue.Dequeue();

                // path was found
                if (currentNode.Equals(goal))
                {
                    return PreparePath(graph, currentNode, root);
                }
                    

                foreach (var neighbour in graph.GetNeighbours(currentNode))
                {
                    if (!visitedNodes.Contains(neighbour))
                    {
                        distanceFromStart++;
                        nodeWeights[neighbour] = distanceFromStart;
                        visitedNodes.Add(neighbour);
                        queue.Enqueue(neighbour);
                    }
                }
            }
            return new List<int>();
        }

        private List<int> PreparePath(Graph graph, int goal, int root)
        {
            List<int> result = new List<int>();
            int currentNode = goal; 
            while (root != currentNode)
            {
                currentNode = MinFromWeights(graph.GetNeighbours(currentNode));
                result.Add(currentNode);
            }
            return result;
        }

        private int MinFromWeights(IList<int> neighbours)
        {
            int minNeighboursIdx = -1;
            int min = 0;
            foreach (var neighbour in neighbours)
            {
                if (min == 0 || nodeWeights[neighbour] < min)
                {
                    min = nodeWeights[neighbour];
                    minNeighboursIdx = neighbour;
                }
            }
            if (min == -1 || minNeighboursIdx == -1) throw new Exception();
            return minNeighboursIdx;
        }
    }
}