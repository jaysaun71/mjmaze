using System;

namespace MJMaze.MazeRepresentation
{
    public interface IMazeSolver<T> : IPathSearchable
    {
        SimplePoint StartPoint { get; set; }
        SimplePoint StopPoint { get; set; }
        T Maze { get; set; }
    }
}