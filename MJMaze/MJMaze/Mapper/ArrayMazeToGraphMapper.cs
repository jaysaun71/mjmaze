﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MJMaze.Mapper
{
    public class ArrayMazeToGraphMapper
    {
        public static Graph ArrayMazeToGraph(int[][] array, int height, int width)
        {
            Graph result = new Graph(height*width);

            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array[i].Length; j++)
                {
                    //if (array[i][j] == 1) continue;
                    int idx = i * width + j;
                    if (array[i][j] == 0)
                    {
                        if (array[i-1][j] == 0) result.AddEdge(idx, (i-1) * width + j);
                        if (array[i+1][j] == 0) result.AddEdge(idx, (i+1) * width + j);
                        if (array[i][j-1] == 0) result.AddEdge(idx, i * width + j - 1);
                        if (array[i][j+1] == 0) result.AddEdge(idx, i * width + j + 1);
                    }
                }
            }

            return result;
        }
    }
}
