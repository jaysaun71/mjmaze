﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MJMaze.MazeRepresentation;

namespace MJMaze.Mapper
{
    public class TextReaderToMazeRepresentationMapper
    {
        public static MazeSolver<int[][]> MazeReprezentationFrom(TextReader textReader)
        {
            int[][] maze;
            int[] headline1;
            int[] headline2;
            int[] headline3;

            char delimiter = ' ';
            string line = String.Empty;

            //header
            line = textReader.ReadLine();
            headline1 = Array.ConvertAll(line.Split(delimiter), int.Parse);
            line = textReader.ReadLine();
            headline2 = Array.ConvertAll(line.Split(delimiter), int.Parse);
            line = textReader.ReadLine();
            headline3 = Array.ConvertAll(line.Split(delimiter), int.Parse);

            //maze array
            maze = new int[headline1[0]][];
            int i = 0;
            while ((line = textReader.ReadLine()) != null)
            {
                maze[i] = Array.ConvertAll(line.Split(delimiter), int.Parse);
                i++;
            }

            // MAP
            var mazeRepresentation = new MazeSolver<int[][]>();
            mazeRepresentation.Size = IntArrayToSimplePointMapper.IntArrayToSimplePoint(headline1);
            mazeRepresentation.StartPoint = IntArrayToSimplePointMapper.IntArrayToSimplePoint(headline2);
            mazeRepresentation.StopPoint = IntArrayToSimplePointMapper.IntArrayToSimplePoint(headline3);
            mazeRepresentation.Maze = maze;

            return mazeRepresentation;
        }
    }
}