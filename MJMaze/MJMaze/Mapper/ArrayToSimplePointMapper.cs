﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MJMaze.MazeRepresentation;

namespace MJMaze.Mapper
{
    public class IntArrayToSimplePointMapper
    {
        public static int[] SimplePointToIntArray(SimplePoint point)
        {
            return new int[]{point.x, point.y};
        }

        public static SimplePoint IntArrayToSimplePoint(int[] intArray)
        {
            if(intArray.Length < 2) throw new ArgumentException();

            return new SimplePoint(){ x=intArray[0],y=intArray[1] };
        }
    }
}
